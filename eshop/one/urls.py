from django.urls import path , include
from django.urls import path, register_converter ,re_path
from . import views , converter

register_converter(converter.FourDigitYearConverter, 'yyyy')
urlpatterns = [
    path('', views.index),
    path('articles/2003/', views.special_case_2003),
    path('archive/<int:year>/', views.year_archive),
    path('archive/<int:year>/<int:month>/', views.month_archive),
    path('archive/<int:year>/<int:month>/<slug:slug>/', views.archive),
    path('custom-url/<yyyy:year>/', views.custom_url),

    # re_path(r'^articles/(?P<year>[0-9]{4})/$', views.year_archive),
    # re_path(r'^articles/(?P<year>[0-9]{4})/(?P<month>[0-9]{2})/$', views.month_archive),
    # re_path(r'^articles/(?P<year>[0-9]{4})/(?P<month>[0-9]{2})/(?P<slug>[\w-]+)/$', views.article_detail),
]
