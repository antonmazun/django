from django.shortcuts import render
from django.http import HttpResponse
# Create your views here.


def index(request):
    return render(request , 'one/index.html' , {})


def special_case_2003(request):
    return HttpResponse('special case for 2003')


def year_archive(request , year):
	print(year)
	if year > 500:
		return HttpResponse('> 500 ')
	else:
		return HttpResponse('< 500')



def month_archive(request , year , month):
    return HttpResponse('Archive for {}/{}'.format(str(year) , str(month)))


def archive(request, year=2003, month=3, slug="building-a-django-site"):
    return HttpResponse('Archive for {}/{}/{}'.format(str(year), str(month) , slug))


def custom_url(request , year):
    return HttpResponse('Custom url for {}'.format(year))