from django.shortcuts import render
from .models import Phone

# Create your views here.


def all_products(request):
	ctx = {}
	all_phones = Phone.objects.all()
	ctx['all_phones'] = all_phones
	return render(request , 'product/all_products.html' , ctx)


def detail_view(request , pk):
	phone = Phone.objects.get(id=pk)
	ctx ={
		'phone_instance': phone
	}
	return render(request , 'product/detail_view.html' , ctx)

