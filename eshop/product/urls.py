from django.urls import path
from .views import all_products , detail_view
urlpatterns = [
	path('' , all_products),
	path('detail-view/<int:pk>' , detail_view)
]